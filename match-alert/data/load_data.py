import json
from urllib.request import urlopen
from bs4 import BeautifulSoup

LINK = "https://www.skysports.com/football-fixtures"
OUTPUT_FILE = "./data/today.json"


def read_page():
    f = urlopen(LINK)
    myfile = f.read()
    return myfile


def parse_html(html):
    parsed_html = BeautifulSoup(html)
    return parsed_html


def write_output(data):
    with open(OUTPUT_FILE, "w") as outfile:
        json.dump(data, outfile)


def main():
    data = []

    # get data from Sky Sports WebSite
    page = read_page()
    parsed_html = BeautifulSoup(page, "lxml")
    matches_list = parsed_html.body.findAll(
        "div", attrs={"class": "fixres__item"})

    # store the data in a dictionary
    for match in matches_list:
        match_time = (
            match.find("span", attrs={"class": "matches__date"})
            .text.replace("\n", "")
            .replace("\t", "")
            .strip()
        )
        match_t1 = (
            match.find("span", attrs={"class": "matches__participant--side1"})
            .text.replace("\n", "")
            .replace("\t", "")
            .strip()
        )
        match_t2 = (
            match.find("span", attrs={"class": "matches__participant--side2"})
            .text.replace("\n", "")
            .replace("\t", "")
            .strip()
        )
        match = {}
        match["home"] = match_t1
        match["away"] = match_t2
        match["time"] = match_time
        data.append(match)

    # write output json
    output = {}
    output["data"] = data
    write_output(output)


main()
