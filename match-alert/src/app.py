import json
from flask import Flask
with open('data/today.json') as f:
    data = json.load(f)

app = Flask(__name__)


@app.route("/")
def hello():
    return str(data)


if __name__ == "__main__":
    app.run("0.0.0.0", port=80, debug=True)
